#!/bin/env node
(function(){
  "use strict";

  var ipaddress = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
  var port      = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;

  var express = require('express');
  var path = require('path');
  var app = express();

  app.use('/public', express.static(path.join(__dirname, 'public')));
  app.use('/', express.static(path.join(__dirname, 'public')));
  app.use('/zohoverify', express.static(path.join(__dirname, 'public')));

  app.get('/public/hero.html', function(req, res){
    res.redirect('public/brad_email/hero.html');
  });

  //app.set('view engine', 'jade');
  app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
  });

  app.get('/test/wechattestvoice', function(req, res) {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
  });

  app.listen(port, ipaddress, function(){
    console.log('server is listening on', 'http://' + ipaddress + ':' + port);
  });

})();
